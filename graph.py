class Node:
    def __init__(self, data=None, parent=None, left=None, right=None):
        self.data = data
        self.parent = parent
        self.left = left
        self.right = right

    def __str__(self):
        _str = str(self.data)
        if self.parent:
            _str += '(' + self.parent.data + ')'
        return _str

    def __eq__(self, other):
        """Перегрузка оператора сравнения"""
        if other is None:
            return False
        return self.data == other.data


class Tree:
    class TreeError(Exception):
        error_msg = 'Ошибка. Неверная скобочная запись'

    def __init__(self, data):
        cur = Node() # Курсор на текущий узел дерева
        self._root = cur # Запоминаем корень
        max_depth = 0 # Максимальная глубина
        cur_depth = 0 # Текущая глубина
        for c in data.replace(' ', ''):
            # Если скобка открывается, 
            # значит дальше пойдет ребенок текущего узла
            if c == '(': 
                # Если у текущего узла еще нет левой ветви
                if not cur.left: 
                    child = Node(parent=cur)
                    cur.left = child
                    cur = child
                    cur_depth += 1
                elif not cur.right:
                    child = Node(parent=cur)
                    cur.right = child
                    cur = child
                    cur_depth += 1
                # Если уже есть и левая и правая, значит, что-то не так
                else:
                    raise self.TreeError
            # Если скобка закрылась, 
            # значит нужно переместить курсор на узел выше
            elif c == ')':
                if not cur.parent:
                    raise self.TreeError
                cur = cur.parent
                cur_depth -= 1
            # Если не скобки, значит данные
            else:
                # Если у текущего узла есть данные, значит это данные соседа
                if cur.data:
                    if not cur.parent.right:
                        right = Node(data=c, parent=cur.parent)
                        cur.parent.right = right
                        cur = right
                    # Если и у соседа уже есть данные, то что-то не так
                    else:
                        raise self.TreeError
                else:
                    cur.data = c
            # Сравниваем стала ли текущая глубина больше максимальной
            max_depth = max(max_depth, cur_depth)
        # Если в конце мы не вернулись к корню, значит запись неверная
        if cur != self._root:
            raise self.TreeError
        self._depth = max_depth

    @property
    def depth(self):
        """Возвращаем глубину которую считали в __init__"""
        return self._depth

    def is_isomorphic(self, other):
        """Проверка на изоморфизм"""
        def _is_isomorphic(a, b):
            if a is None and b is None:
                return True
            elif a is not None and b is not None:
                # Если у одного из деревьев есть одинаковые ветви, 
                # А у второго нет, то это не изоморфизм
                if (a.right == a.left and b.right != b.left or
                        b.right == b.left and a.right != a.left):
                    return False
                # Иначе сравниваем 
                else:
                    return (
                        a == b and
                        (
                            _is_isomorphic(a.right, b.right) or
                            _is_isomorphic(a.right, b.left)
                        ) and
                        (
                            _is_isomorphic(a.left, b.left) or
                            _is_isomorphic(a.left, b.right)
                        )
                )
            # Один из узлов пуст, а второй нет,
            # Значит не изоморфизм
            return False
        # Вызов функции выше
        return _is_isomorphic(self._root, other._root)

    def print_self(self):
        """Напечатать дерево по уровням"""
        def _print_self(print_list):
            # Список, исключая текущий уровень
            next_lvl = list() 
            for i in print_list:
                # Если не список, то это наш уровень
                if type(i) is not list:
                    print(i, end=' ')
                else:
                    next_lvl += i
            print() # Выводит разрыв строки
            if next_lvl:
                _print_self(next_lvl)
        # Вызываем рекурсивную функцию выше
        _print_self(self.recursive_descent(self._root))

    def recursive_descent(self, root):
        """Рекурсивно проходим дерево и составляем из него список"""
        print_list = [str(root), ]
        if root.left:
            print_list += [self.recursive_descent(root=root.left), ]
        if root.right:
            print_list += [self.recursive_descent(root=root.right), ]
        # Можно раскомментировать, если интересно как список выглядит
        # print('Список: {}'.format(print_list))
        return print_list
