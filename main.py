from graph import Tree


def main():
    data1, data2 = None, None
    with open('text.txt', 'r') as f:
        # Читаем файл построчно
        filedata = f.read().splitlines()
        if filedata:
            data = filedata[0]
        else:
            print('Файл пуст!')
            return
        # Если 2 строки
        if len(filedata) >= 2:
            data2 = filedata[1]
    try:
        # Инициализируем первое дерево
        tree = Tree(data)
        print('Дерево:')
        # Печатаем дерево
        tree.print_self()
        # Печатаем глубину дерева
        print('Глубина = {}'.format(tree.depth))
        # Если в файле было 2 строки, 
        # то инициализируем второе дерево
        # и сравниваем их на изоморфизм
        if data2:
            print('Дерево 2:')
            other_tree = Tree(data2)
            other_tree.print_self()
            print(
                'Изоморфизм: {}'.format(
                    tree.is_isomorphic(other_tree)
                )
            )
    # Если возникают какие-либо ошибки связанные
    # С неправильной линейно-скобочной записью
    # т.е неверным значением в файле то выводим сообщение о ошибке
    except Tree.TreeError as e:
        print(e.error_msg)

if __name__ == "__main__":
    main()
